import pygame

pygame.init()

# nastavení okna
screen = pygame.display.set_mode((640, 360), pygame.FULLSCREEN)  # velikost okna
pygame.display.set_caption("Girard the Divine")   # název okna
icon = pygame.image.load("graphic/ico.png")    # ikona programu
pygame.display.set_icon(icon)

# grafika ve hře
logo = pygame.image.load("graphic/HACKER_NINJAS_north.png") # logo ninjas
walkRight = [pygame.image.load("graphic/king/R1.png"), pygame.image.load("graphic/king/R2.png"), pygame.image.load("graphic/king/R3.png"), pygame.image.load("graphic/king/R4.png"), pygame.image.load("graphic/king/R5.png"), pygame.image.load("graphic/king/R6.png"), pygame.image.load("graphic/king/R7.png"), pygame.image.load("graphic/king/R8.png")]   # seznam obrázků chůze doprava, načtení obrázků z jiné složky -> pygame.image.load("nazev_slozky/R9.png")
walkLeft = [pygame.image.load("graphic/king/L1.png"), pygame.image.load("graphic/king/L2.png"), pygame.image.load("graphic/king/L3.png"), pygame.image.load("graphic/king/L4.png"), pygame.image.load("graphic/king/L5.png"), pygame.image.load("graphic/king/L6.png"), pygame.image.load("graphic/king/L7.png"), pygame.image.load("graphic/king/L8.png")]    # seznam obrázků chůze doleva
jumpL = [pygame.image.load("graphic/king/JL.png")]
jumpR = [pygame.image.load("graphic/king/JR.png")]
attackR = [pygame.image.load("graphic/king/AR1.png"), pygame.image.load("graphic/king/AR1.png"), pygame.image.load("graphic/king/AR1.png"), pygame.image.load("graphic/king/AR2.png"), pygame.image.load("graphic/king/AR2.png"),pygame.image.load("graphic/king/AR3.png"),pygame.image.load("graphic/king/AR3.png"),pygame.image.load("graphic/king/AR3.png")]
attackL = [pygame.image.load("graphic/king/AL1.png"), pygame.image.load("graphic/king/AL1.png"), pygame.image.load("graphic/king/AL1.png"), pygame.image.load("graphic/king/AL2.png"), pygame.image.load("graphic/king/AL2.png"),pygame.image.load("graphic/king/AL3.png"),pygame.image.load("graphic/king/AL3.png"),pygame.image.load("graphic/king/AL3.png")]
bg = pygame.image.load("graphic/bg.jpg")    # obrázek pozadí

clock = pygame.time.Clock() # framerate hry

# zvuky
pygame.mixer.music.load("sound/level.mp3")
pygame.mixer.music.play(-1)

# třídy
class Player(object):
    """
    třída postavy hráče
    """
    def __init__(self, x, y, width, height):
        """
        atributy třídy
        """
        self.x = x  # výchozí bod na ose x
        self.y = y  # výchozí bod na ose y
        self.width = width  # šířka postavy
        self.height = height    # výška postavy
        self.vel = 3    # počet bodů posunu
        self.isJump = False  # proměnná skákání
        self.jumpCount = 7  # počet bodů skoku
        self.left = False   # proměnná pohybu doleva
        self.right = False  # proměnná pohybu doprava
        self.walkCount = 0  # počet snímků pohybu
        self.stand = True
        self.attack = False
        self.attack_frame = 0

    def attack(self):        
      """
      nastavení útoku hráče
      """
      if self.attack_frame > 8:    # Pokud útočný snímek dosáhl konce sekvence, vrátí se do základního snímku  
            self.attack_frame = 0
            self.attack = False 
      
      if self.right:# Check direction for correct animation to display  
            self.image = attackR[self.attack_frame]
      elif self.left:
            self.image = attackL[self.attack_frame] 
 
      # Update the current attack frame  
      self.attack_frame += 1
        
    def draw(self,screen):
        """
        definice animace hráče
        """
        if self.walkCount + 1 >= 24: # pohyb doprava a doleva má 8 obrázků a každý se bude zobrazovat 3 snímky -> 8 * 3 = 24 ,  animace pohybu
            self.walkCount = 0

        if not (self.stand):
            if self.right:
                screen.blit(walkRight[self.walkCount//3], (self.x,self.y))
                self.walkCount += 1
            elif self.left:
                screen.blit(walkLeft[self.walkCount//3], (self.x,self.y))
                self.walkCount += 1
            elif self.isJump:
                self.walkCount += 1             
               
        else:   # postava zůstane otočená směrem kudy šla
            if self.right:
                screen.blit(walkRight[0], (self.x,self.y))
            elif self.left:
                screen.blit(walkLeft[0], (self.x, self.y))
            else:
                screen.blit(walkRight[0], (self.x, self.y)) # výchozí otočení

class Enemy(object):
    """
    Reprezentuje nepřátelská prasátka
    """
    walkRight = [pygame.image.load("graphic/pigs/R1.png"), pygame.image.load("graphic/pigs/R2.png"), pygame.image.load("graphic/pigs/R3.png"), pygame.image.load("graphic/pigs/R4.png"), pygame.image.load("graphic/pigs/R5.png"), pygame.image.load("graphic/pigs/R6.png")]
    walkLeft = [pygame.image.load("graphic/pigs/L1.png"), pygame.image.load("graphic/pigs/L2.png"), pygame.image.load("graphic/pigs/L3.png"), pygame.image.load("graphic/pigs/L4.png"), pygame.image.load("graphic/pigs/L5.png"), pygame.image.load("graphic/pigs/L6.png")]

    def __init__(self, x, y, width, height, end):   # end je koncový bod trasy chůze nepřítele
        """
        hodnoty postavy prasátek
        """
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.path = [x, end]
        self.walkCount = 0
        self.vel = 1
    def draw(self, screen):
        """
        definice animace prasátek
        """
        self.move()
        if self.walkCount + 1 >= 18:
            self.walkCount = 0
        
        if self.vel > 0:
            screen.blit(self.walkRight[self.walkCount//3], (self.x,self.y))
            self.walkCount += 1
        else:
            screen.blit(self.walkLeft[self.walkCount//3], (self.x,self.y))
            self.walkCount += 1
    def move(self):
        """
        definice pochodování prasete
        """
    def move(self):
        if self.vel > 0:
            if self.x < self.path[1] + self.vel:
                self.x += self.vel
            else:
                self.vel = self.vel * -1
                self.x += self.vel
                self.walkCount = 0
        else:
            if self.x > self.path[0] - self.vel:
                self.x += self.vel
            else:
                self.vel = self.vel * -1
                self.x += self.vel
                self.walkCount = 0
          
def redrawGameWindow(): # funkce překreslení okna při pohybu postavy
    screen.blit(bg, (0, 0)) # vyplnění okna obrázkem bg od souřadnice 0, 0
    king.draw(screen)
    pig.draw(screen)
    pygame.display.update() # obnovení displeje

# hlavní smyčka hry
king = Player(200, 340, 78, 58) # výchozí souřadnice postavy hráče a její velikost
pig = Enemy(100, 355, 34, 28, 300)   # výchozí souřadnice postavy prasete, její velikost a koncoví bod na ose x
run = True
while run:
    clock.tick(30)   # framerate v FPS
    for event in pygame.event.get():
        if event.type == pygame.QUIT:   # po kliknutí na X přeruší smyčku a zavře okno
            run = False

    keys = pygame.key.get_pressed() # keys = stisknutí klávesy ......
   # pohyb do stran
    if keys [pygame.K_LEFT] and king.x > king.vel:  # pohyb doleva a ošetření uníku mimo obrazovku
        king.x -= king.vel    # pohyb doleva odečítá na ose x rychlostí vel
        king.left = True
        king.right = False
        king.stand = False
        king.attack = False
    elif keys [pygame.K_RIGHT] and king.x < 640 - king.width - king.vel:   # pohyb doprava a ošetření uníku mimo obrazovku. (šiřka okna - šířka objektu - body posunu)
        king.x += king.vel    # pohyb doprava přičítá na ose x rychlostí vel
        king.right = True
        king.left = False
        king.stand = False
        king.attack = False
    else:   # pokud se nehýbeme
        king.stand = True
        king.walkCount = 0
    # pohyb skok
    if not (king.isJump):
        if keys [pygame.K_UP]:   # skok postavy
            king.isJump = True
            king.right = False
            king.left = False
            king.stand = False
            king.attack = False
            king.walkCount = 0
    
    else:   # definice skoku
        if king.jumpCount >= -7:
            king.y -= (king.jumpCount * abs(king.jumpCount)) * 0.5
            king.jumpCount -= 1
        else:
            king.isJump = False
            king.jumpCount = 7

    # pohyb útok
    if keys [pygame.K_SPACE]:
        king.attack = True
        king.left = False
        king.right = False
        king.stand = False
        
    
    redrawGameWindow()  # volání funkce překreslení okna
    


pygame.guit()